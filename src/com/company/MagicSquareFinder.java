package com.company;

import java.util.Scanner;

public class MagicSquareFinder {

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int size = scan.nextInt();
        int[][] matrix = new int[size][size];
        for(int row=0;row<size;row++)
        {
            for(int col=0;col<size;col++)
            {
                matrix[row][col] = scan.nextInt();
            }
        }
        int forwardDiagonalSum=0;
        int backwardDiagonalSum=0;
        for(int row =0;row<size;row++)
        {
            forwardDiagonalSum += matrix[row][row];
            backwardDiagonalSum += matrix[row][size-row-1];
        }
        if(forwardDiagonalSum == backwardDiagonalSum)
        {
            int rowSum=0,colSum=0,row;
            for( row=0;row<size;row++)
            {
                for(int col=0;col<size;col++)
                {
                    rowSum += matrix[row][col];
                    colSum += matrix[col][row];
                }
                if(rowSum!=colSum || rowSum!=forwardDiagonalSum)
                {
                    break;
                }
            }
            if(row==size)
            {
                System.out.println("yes");
            }
            else
            {
                System.out.println("No");
            }
        }
        else
        {
            System.out.println("No");
        }
    }
}
